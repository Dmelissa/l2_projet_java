package ulco.cardGame.common.games;

import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.BoardPlayer;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.net.Socket;

public abstract class BoardGame implements Game {

    protected String name;
    protected Integer maxPlayers;
    protected List<Player> players;
    protected boolean endGame;
    protected boolean started;
    protected Board board;

    /**
     * Enable constructor of Game
     * - Name of the game
     * - Maximum number of players of the Game
     * - Filename with all required information to load the Game
     * @param name
     * @param maxPlayers
     * @param filename
     */
    public BoardGame(String name, Integer maxPlayers, String filename) {
        this.name = name;
        this.maxPlayers = maxPlayers;
        this.endGame = false;
        this.players = new ArrayList<>();

        // initialize the Board Game using
        this.initialize(filename);
    }

    /**
     * Add new players inside the game only if possible
     * @param player
     */
    @Override
    public boolean addPlayer(Socket socket, Player player) throws IOException {
        // check number of authorized players in the game
        if (players.size()>=maxPlayers) {
            try {
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                oos.writeObject("Maximum number of players already reached ");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        for (Player p : players) {
            if (p.getName().equals(player.getName())) {
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject("assigned");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return false;
            }
        }

        players.add((BoardPlayer)player);
        System.out.println("Player added into players game list");
        return true;

    }

    @Override
    public Player getCurrentPlayer(String name) {
        for (Player player : players) {
            if (player.getName().equals(name)) {
                return player;
            }
        }
        return null;
    }



    /**
     * Remove player from the game using the reference
     * @param player
     */
    @Override
    public void removePlayer(Player player) {
        // not forget to disconnect the player
        this.players.remove(player);
    }

    /**
     * Remove players from the game
     */
    @Override
    public void removePlayers(){
        // not forget to disconnect the player
        this.players.clear();
    }

    @Override
    public void displayState() {

        // Display Game state
        System.out.println("-------------------------------------------");
        System.out.println("--------------- Game State ----------------");
        System.out.println("-------------------------------------------");
        for (Player currentPlayer : players) {
            System.out.println(currentPlayer);
        }
        System.out.println("-------------------------------------------");
    }

